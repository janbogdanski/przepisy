# Makowy sernik na spodzie brownie, z orzechami, konfiturą żurawinową i bezą włoską

## Składniki (forma 24cm)

Spód brownie:

* 170g masła
* 170g gorzkiej czekolady (70%+)
* 3 jajka (M)
* 180g cukru
* 115g mąki pszennej
* szczypta soli
* 60g posiekanych orzechów włoskich

Masa serowa:

* 500g sera (zmielonego 3x)
* 100g serka mascarpone
* 1,5 łyżki mąki ziemniaczanej
* 170g drobnego cukru
* 3 jajka (M)
* 85g śmietany kremówki 30-36% (mniej jak ser z kubła)
* 60g syokiego **mielonego** maku
* skórka i sok z 1 cytryny

Konfitura żurawinowa:

* 200g żurawiny świeżej
* 1 jabłko starte na drobnych oczkach
* sok z 1/2 cytryny
* 100 - 140g cukru

Beza włoska:

* 3 białka
* 230g cukru (200 + 30)
* 45ml wody

## Wykonanie
### Spód brownie
Piekarnik rozgrzaćdo 160st C (bez termoobiegu).
Dno formy o średnicy 22-24cm wyłożyć papierem do pieczenia.
Masło pokroić na kawałki, włożyć do rondla i roztopić mieszającna małym ogniu.
Dodać pokruszoną czekoladę w połowie roztapiania masła, mieszać,  nie nagrzewać, zdjąć z palnika.
W oddzielnej misce roztrzepać mikserem jajka z cukrem, dodać roztopioną masę czekoladową, zmiksować na jednolitą masę.
Dodać mąkę, sól, zmiksować do połączenia.
Dno formy wyłożyć papierem do pieczenia, wyłożyć masę, wyrównać powierzchnię i posyoać posiekanymi orzechami.
Włożyć do piekarnika, piec 25 minut.

### Masa serowa
Piekarnik nagrzaćdo 175 stopni.
Do misy miksera wlożyć ser i mascarpone, mak, sok i skórkę z cytryny i mąkę.
Ucierać przez 2-3min na **średnich lub niskich obrotach** aż masa będzie gładka.
Stopniowo dodawać cukier cały czas miksując na małych obrotach - nie napowietrzać masy przez szybkie obroty.
Wbijaj kolejno jajka miksując 30s po każdym dodanym jajku.
Na koniec zmiksuj ze śmietaną.
Masę delikatnie wylej na spód, wyrównaj powierzchnię i ustaw na środkowej kratce piekarnika.
Piec 10 min następnie zmniejszyć temperaturę do 120 stopni i piec 30-35 min - środek może być luźniejszy.
Sernik wyjmować stopniowo, schłodzić w lodówce.

### Konfitura źurawinowa
Zasyp żurawinę cukrem i delikarnie mieszaj, żeby rozprowadził się po wszystkich owocach.
Dodać starte jabłko, sok z cytryny u podgrzewaj do rozpuszczenia cukru, następnie zagotuj.
Przesmażać, od czasu do czasu mieszając przez 20-30 min.
Kiedy zacznie ciemnieć, sprawdź gęstość na talerzyku zmrożonym przez 1 minutę w zamrażalniku. Kropla konfitury powinna utrzymać się bez rozlewania, może być rzadsze niż drzem/marmolada.

### Beza włoska
W małym garnku wymieszać cukier (200g) i wodę.
Ustaw na palniku, zagotuj **nie mieszać**. Włóż termometr.
Gotować na średnim ogniu do temperatury 118 stopni.
W tym czasie ubić białka na sztywno.
Na koniec ubijania dodać stopniowo 30g cukru.

Po osiągnięciu temperatury (test pióra) zdjąć garnek z palnika, **zwiększyć obroty na maksymalne** i cienkim strumieniem wlewać syrop cukrowy..
Mieszać przez 6-10min aż do ochłodzenia do temp 45 stopni.
Beza powinna być sztywna, błyszcząca. Przełożyć pianę do rękawa i grubszą tylką udekorować sernik.
Opalić palnikiem cukierniczym (lub grillem w piekarniku)